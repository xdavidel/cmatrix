CC=gcc
CFLAGS=-g -Wall -Iinclude

SRCDIR=src
OBJDIR=obj

SRCS=$(wildcard $(SRCDIR)/*.c)
OBJS=$(patsubst $(SRCDIR)/%.c, $(OBJDIR)/%.o, $(SRCS))

BINDIR=bin
BIN=$(BINDIR)/cmatrix
SUBMITNAME=proj.zip

ifdef OS
	CCMD=del /q $(BINDIR)\* $(OBJDIR)\*
else
	CCMD=rm -rf $(BINDIR)/* $(OBJDIR)/*
endif

all: $(BIN)

release: CFLAGS=-Wall -O2 -DNDEBUG -Iinclude
release: clean
release: $(BIN)

$(BIN): $(OBJS) | bin
	$(CC) $(CFLAGS) $(OBJS) -o $@ -lncurses

$(OBJDIR)/%.o: $(SRCDIR)/%.c | obj
	$(CC) $(CFLAGS) -c $< -o $@

bin obj:
	mkdir "$@"

clean:
	$(CCMD)

submit:
	$(RM) $(SUBMITNAME)
	zip $(SUBMITNAME) $(BIN)