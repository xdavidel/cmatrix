#include <string.h>
#include <assert.h>
#include <curses.h>
#include <unistd.h>

#include "ui.h"

WINDOW *uiwindow = NULL;

int color_map[MAX_INTESITY + 1] = {1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 4, 5, 3, 6};

void set_colors();

bool init_ui() {

    uiwindow = initscr();
    if (uiwindow == NULL) return false;

    start_color();
    if (!has_colors() || !can_change_color() || COLOR_PAIRS < 6) {
        puts("Warning: your terminal cannot handle this program");
        return false;
    }
    
    set_colors();
    
    return true;
}

void cleanup_ui() {
    delwin(uiwindow);
    endwin();
    refresh();
}

void show_matrix() {
    for (int x = 0; x < MAXX; x++) {
        for(int y = 0; y < MAXY; y++) {
            int intesity = matrix[x][y].intesity;
            color_set(color_map[intesity], NULL);
            mvaddch(y, x, matrix[x][y].char_value);
        }
    }
    
    refresh();
}


void set_colors() {
    int i;
    for(i = 0; i < 8; i++){
        init_pair(i+1, i, COLOR_BLACK);
    }

    for(i = 0; i <= 5; i++){
        init_color(i, 0, i*200, 0);
    }
    init_color(i, 800, 1000, 800);
}