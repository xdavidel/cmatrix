#ifndef _UI_H
#define _UI_H

#include <stdbool.h>

#define MAXX 160
#define MAXY 50

#define MAX_INTESITY 13
#define MIN_INTESITY 2

typedef struct _cell_t {
    char char_value;
    int intesity;
} cell_t;


/* define elsewhere */
extern cell_t matrix[MAXX][MAXY];

bool init_ui();
void cleanup_ui();
void show_matrix();

#endif